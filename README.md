# ## OBS NEEDS REFACTORING ## #

# README #

# Welcome

Welcome to the Plankplaneraren wiki! Here you can find everything that is needed to understand and run Plankplaneraren.

Have fun!

### How do I get set up? ###

The project, Plankplaneraren, contains two components, the server and the client.
The server is written in Node.js using Express framwork, this should be changed. We should use a more REST oriented framework, but express for now.

The server depends on you having a mongo service running on the default mongo port, 27017.

**To start the server:**
*npm install && npm install -g && grunt*

**The Androd Client:
**To start the Android Application, just import the app into Android Studio.
IMPORTANT! - The emulator must be the default emulator, supplied by android. AND you must run a Google API 19+ target, due to Google play services.

### How do I emulate getting a text or changing GPS location? ###
If you use the default Android Emulator this will be easy. Telnet to localhost 5554(it's says in the window title of the emulator).

**SMS:** 
*sms send <provider> <message>*

**GPS:**
*geo fix <long> <lat>*


To emulate a incoming Ticket send the message "Ticket". And to emulate a Validation ticket send the message "Validation"